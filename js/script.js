// Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.
// setTimeout() - викликає функцію через встановлений проміжок часу.
// setInterval() - викликає функцію регулярно через встановлений проміжок часу, тобто є інтервали.

// Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
// Вона не спрацює миттєво, адже спершу виконується поточний код а потім вже функція. Там буде затримка приблизно 4 мілісекунди.

// Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
//Для того щоб навіть у так би мовити у фоновому режимі цей цикл не працював треба викликати clearInterval().


const imgArray = document.querySelectorAll('.image-to-show');
const btnStop = document.querySelector('.btn-stop');
const btnContinue = document.querySelector('.btn-continue');
const timer = document.querySelector('.timer');
let counter = 0;
let timerStart = 3000;


function showImg() {
    timerStart -= 40;
    let seconds = (timerStart / 1000).toFixed(2);
    timer.innerText = `${seconds}`;
    if(!timerStart) {
        ++counter;
        imgArray.forEach(elem => {
            elem.classList.remove('active');
            if(+elem.dataset.order === counter % imgArray.length)
            elem.classList.add('active');
        });
        timerStart = 3000;
    }
}

document.addEventListener('DOMContentLoaded', ev => {
    show = setInterval(showImg, 40);
})

btnStop.addEventListener('click', evnt => {
    clearInterval(show);
})

btnContinue.addEventListener('click', evnt => {
    show = setInterval(showImg, 40);
})



